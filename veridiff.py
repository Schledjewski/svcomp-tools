#!/usr/bin/env python3.6
import os
from subprocess import run,PIPE
import json
from random import randint
import os.path
from enum import Enum

# tools: the name of the xml files (without the suffix)
tools = {"cbmc":"/tools/cbmc","seahorn":"/tools/seahorn/bin","utaipan":"/tools/utaipan","uautomizer":"/tools/uautomizer"}

paths = os.environ["PATH"].split(os.pathsep)
pathsToAdd = [path for tool,path in tools.items() if not path in paths]
os.environ["PATH"] += os.pathsep + os.pathsep.join(pathsToAdd)


outputdir = '/var/benchexec/output/'

class STATUS(Enum):
    UNKNOWN = 0
    REACHABLE = 1
    UNREACHABLE = 2

def run_benchexec(subdir):
    print("running benchexec\n")
    if not os.path.exists(outputdir+subdir):
        run(["mkdir","-p",outputdir+subdir+"results/"],cwd=outputdir, stdout=PIPE)
        for tool,path in tools.items():
                command = ["benchexec","--container","--full-access-dir","/","/tools/"+tool+".xml","-o",outputdir+subdir+"results/"]
                print("\n\n\n===== Running %s =====\n" % tool)
                run(command ,cwd=path)
    else:
        files = [f for f in os.listdir(outputdir+subdir) if os.path.isfile(os.path.join(outputdir+subdir,f))]
        for tool,path in tools.items():
            toolFiles = [f for f in files if f.startswith(tool) and f.endswith(".txt")]
            if not toolFiles:
                command = ["benchexec","--container","--full-access-dir","/","/tools/"+tool+".xml","-o",outputdir+subdir+"results/"]
                print("\n\n\n===== Running %s =====\n" % tool)
                run(command ,cwd=path)


def getInfoForFile(filepath,filename):
    command = ["veridiff","-j",filename+".json","-i",filename+".dump.c",filepath]
    run(command ,cwd=outputdir, stdout=PIPE)
    with open(outputdir+filename+'.json') as f:
        file = json.load(f)
        return (file["numberOfReads"],file["numberOfWrites"])

def createRandomValue():
    value =""
    values="0123456789ABCDEF"
    for i in range(16):
        value += values[randint(0,15)]
    return value

def generateRandomInstrumentationFile(filename):
    command = ["generator","-j",filename+".json","initial.json"]
    run(command ,cwd=outputdir, stdout=PIPE)
    with open(outputdir+'initial.json') as f:
        return json.load(f)



def updateFileSet(fileName):
    with open("ReachSafety.set",'w') as f:
        f.write(outputdir+fileName)

def instrument(jsonData,fileName,filePath):
    with open(outputdir+"initial.json",'w') as f:
        json.dump(jsonData,f)
    instrumentedFileName = fileName +"_"+ ("read" if jsonData["isRead"] else "write") +str(jsonData["number"])+"_"+jsonData["value"]+".c"
    command = ["veridiff","-j","dummy.json","-a","initial.json","-i", instrumentedFileName,filePath]
    run(command ,cwd=outputdir, stdout=PIPE)
    return instrumentedFileName

def getStatus(resultDir):
    files = [f for f in os.listdir(resultDir) if os.path.isfile(os.path.join(resultDir,f))]
    returnValues = dict()
    for tool,path in tools.items():
        toolFiles = [f for f in files if f.startswith(tool)]
        resultFile = [f for f in toolFiles if f.endswith(".txt")][0]
        with open(os.path.join(resultDir,resultFile)) as f:
            lines = f.read().splitlines()
            if lines[-2][-1] == "1":
                returnValues[tool] = STATUS.UNKNOWN
            else:
                if lines[-3][-1] == "1" or lines[-6][-1] == "1":
                    returnValues[tool] = STATUS.REACHABLE
                else:
                    returnValues[tool] = STATUS.UNREACHABLE
    return returnValues

filename = "array_false-unreach-call_true-termination.c"
filePath = "/var/benchexec/input/"+filename
numReads,numWrites = getInfoForFile(filePath, filename)

positionsCounter = 0
maxPositions = 10

instrumentNewPosition = True
while instrumentNewPosition:
    jsonData = generateRandomInstrumentationFile(filename)
    instrumentedFileName = instrument(jsonData,filename,filePath)
    updateFileSet(instrumentedFileName)
    run_benchexec(instrumentedFileName+".dir/")
    resultDir = outputdir+instrumentedFileName+".dir/results/"
    status = getStatus(resultDir)
    # check whether we want to go to a new position
    positionsCounter += 1
    if positionsCounter == maxPositions:
        instrumentNewPosition = False
    # check whether we want to stay at this position and just update the value
    instrumentSamePosition = True
    returnCodes = {code for tool,code in status.items()}
    if len(returnCodes) == 1:
            instrumentSamePosition = False
    while instrumentSamePosition:
        jsonData["value"]=createRandomValue()
        instrumentedFileName = instrument(jsonData,filename,filePath)
        updateFileSet(instrumentedFileName)
        run_benchexec(instrumentedFileName+".dir/")
        resultDir = outputdir+instrumentedFileName+".dir/results/"
        status = getStatus(resultDir)
        # decide whether we stay at this position
        instrumentSamePosition = False
        returnCodes = {code for tool,code in status.items()}
        if len(returnCodes) == 1:
            instrumentSamePosition = False
        else:
            instrumentSamePosition = True #TODO different situtations: all three, one of them is UNKNOWN or they contradict
        if randint(1,100) > 95:
            instrumentSamePosition = False #make sure that we do not get stuck
        
